/*
 * CCSprite9Slice.h
 *
 * Created by Six Foot Three Foot on 9/04/13.
 * Copyright (c) Six Foot Three Foot 2013. All rights reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 */

#import "cocos2d.h"

@interface CCSprite9Slice : CCSprite {//CCNode <CCRGBAProtocol> {
}

/** initialize a new sprite from a file, with an inset of x pixels */
-(id) initWithFile:(NSString *)fileName inset:(CGSize)inset;

+(CCSprite9Slice *) spriteWithFile:(NSString *)file inset:(CGSize)inset;

+(CCSprite9Slice *) spriteWithSpriteFrameName:(NSString *)frameName inset:(CGSize)inset;
/** Transform the rectangular shape */
-(void) setSize:(CGSize)size;

@end